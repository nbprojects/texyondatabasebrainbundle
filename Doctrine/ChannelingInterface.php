<?php

namespace Texyon\Database\BrainBundle\Doctrine;

use Texyon\Database\BrainBundle\Doctrine\DoctrineDomainManagerInterface;

/**
 * Interface ChannelingInterface
 * @package Texyon\Database\BrainBundle\Entity\Channeling
 */
interface ChannelingInterface extends DoctrineDomainManagerInterface
{

}
