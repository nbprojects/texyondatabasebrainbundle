<?php

namespace Texyon\Database\BrainBundle\Doctrine\Channeling;

use Texyon\Database\BrainBundle\Entity\Accounts;

/**
 * Class LevelCalculator
 * @package Texyon\Database\BrainBundle\Doctrine
 */
class LevelCalculator
{
    const INCREASE_LEVEL = 1;
    const DEFAULT_LEVEL = 1;
    /** @var  Accounts */
    private $parent;
    /** @var  string */
    private $channelName;

    /**
     * @param null|Accounts $parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @param string $channelName
     *
     * @return $this
     */
    public function setChannelName($channelName)
    {
        $this->channelName = $channelName;

        return $this;
    }

    /**
     * @param Accounts $account
     *
     * @return int
     */
    public function getLevel()
    {
        if ($this->parent instanceof Accounts) {
            if ($this->parent->getChanneling() instanceof channeling) {
                if (!is_null($this->channelName)) {
                    if ($this->channelName !== $this->parent->getChanneling()->getChannel()) {
                        return self::DEFAULT_LEVEL;
                    }
                }

                return $this->parent->getChanneling()->getLevel() + self::INCREASE_LEVEL;
            }
        }

        return self::DEFAULT_LEVEL;
    }

}
