<?php

namespace Texyon\Database\BrainBundle\Doctrine\Channeling;

use Texyon\Database\BrainBundle\Entity\Accounts;
use Texyon\Database\BrainBundle\Entity\Channeling as channelingEntity;

/**
 * Class CreateChannelingFromParent
 * @package Texyon\Database\BrainBundle\Doctrine
 */
class CreateChannelingFromParent
{
    /** @var  Accounts */
    private $parent;

    private $chanelName;

    /**
     * @param string $chanelName
     */
    public function setChanelName($chanelName)
    {
        $this->chanelName = $chanelName;

        return $this;
    }

    /**
     * @param Accounts $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return string
     */
    private function getChannelName()
    {
        if (is_null($this->chanelName)) {
            if ($this->parent instanceof Accounts) {
                if (!is_null($this->parent->getChanneling())) {
                    return $this->parent->getChanneling()->getChannel();
                }
            }
        }

        return $this->chanelName;
    }

    /**
     * @return channelingEntity
     */
    public function createChannel()
    {
        $channelName = $this->getChannelName();
        $level = $this->getLevel();

        if (!is_null($channelName)) {
            return $this->create($channelName, $level);
        }
    }

    /**
     * @param string $channelName
     * @param int    $level
     *
     * @return channelingEntity
     */
    private function create($channelName, $level)
    {
        $channeling = new channelingEntity();
        $channeling->setNewChanneling($channelName, $level);

        return $channeling;
    }

    /**
     * @return int
     */
    private function getLevel()
    {
        $level = new LevelCalculator();

        return $level
            ->setParent($this->parent)
            ->setChannelName($this->chanelName)
            ->getLevel();
    }
}
