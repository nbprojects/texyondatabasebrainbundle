<?php

namespace Texyon\Database\BrainBundle\Doctrine;

use Doctrine\ORM\EntityManager;
use Texyon\Database\BrainBundle\Entity\Model;

/**
 * Class DoctrineDomainManager
 * @package Texyon\Database\BrainBundle\Doctrine
 */
class DoctrineDomainManager
{
    /** @var \Doctrine\ORM\EntityManager  */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Persist entity.
     *
     * @param ModelInterface $entity
     *
     * @return int $id;
     */
    public function save(ModelInterface $entity)
    {
        $entity->configure();
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity->getId();
    }

    /**
     * Remove entity
     *
     * @param ModelInterface $entity
     */
    public function delete(ModelInterface $entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}
