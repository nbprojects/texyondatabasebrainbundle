<?php

namespace Texyon\Database\BrainBundle\Doctrine;

use Texyon\Database\BrainBundle\Entity\Model;
use Doctrine\ORM\EntityRepository;

/**
 * Interface DoctrineDomainManagerInterface
 * @package Texyon\Database\BrainBundle\Doctrine
 */
interface DoctrineDomainManagerInterface
{
    /**
     * @return EntityRepository
     */
    public function getRepository();

    /**
     * Persist entity.
     *
     * @param ModelInterface $entity
     *
     * @return int $id;
     */
    public function save(ModelInterface $entity);

    /**
     * Remove entity
     *
     * @param ModelInterface $entity
     */
    public function delete(ModelInterface $entity);
}
