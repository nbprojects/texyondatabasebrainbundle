<?php

namespace Texyon\Database\BrainBundle\Doctrine;

use Texyon\Database\BrainBundle\Doctrine\DoctrineDomainManagerInterface;

/**
 * Interface CookiesInterface
 * @package Texyon\Database\BrainBundle\Model\Cookies
 */
interface CookiesRefererInterface extends DoctrineDomainManagerInterface
{

}
