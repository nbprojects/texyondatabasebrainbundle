<?php

namespace Texyon\Database\BrainBundle\Lib;

class Tools
{

    public static function partition_dir($name,$base = false)
    {
        $name = substr(md5($name),0,8);
        $route = implode("/",str_split($name,2));
        if($base) // si se define la base, se crea el directorio... si no tan solo se devuelve la ruta
            if(!is_dir($base.$route)) mkdir($base.$route,0755,true);        // creacion de directorio d modulo!

        return $route."/";
    }

    public function createSlug($str, $max=30)
    {
        $out = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $out = substr(preg_replace("/[^-\/+|\w ]/", '', $out), 0, $max);
        $out = strtolower(trim($out, '-'));
        $out = preg_replace("/[\/_| -]+/", '-', $out);

        return $out;
    }
}
