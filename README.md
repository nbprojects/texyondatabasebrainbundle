texyonDatabaseBrainBundle
====================

## Instalation.

### 1. With composer.


Add in your composer.json:

```json
{
    "require": {
        "texyon/database-brain-bundle": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "git@github.com:TEXYONGAMES/texyonDatabaseBrainBundle.git"
        }
    ],
    "config": {
        "bin-dir": "bin",
        "github-oauth":{
            "github.com":"c1e90cd60562ff8f75d1b2bee5c69a14a8d4b6ec"
        }
    }
}
```

### 2. Download the bundle:

  ```bash
  $php composer.phar update texyon/database-brain-bundle
  ```

### 3. Enable the bundle:
 In your AppKernel.php

 ```php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
         new Texyon\Database\BrainBundle\TexyonDatabaseBrainBundle(),
    );
}
```
