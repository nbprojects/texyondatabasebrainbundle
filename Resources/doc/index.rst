Getting Started With TexyonDatabaseBrainBundle
===============================================

The Symfony Security component provides a flexible security framework that
allows you to load users from configuration, a database, or anywhere else
you can imagine. The FOSUserBundle builds on top of this to make it quick
and easy to store users in a database.

So, if you need to persist and fetch the users in your system to and from
a database, then you're in the right place.

Prerequisites
-------------

This version of the bundle requires Symfony 3.0+.

Installation
------------

Installation is a quick (I promise!) 7 step process:

1. Configure the composer.json file
2. Download TexyonDatabaseBrainBundle using composer
3. Enable the Bundle
4. Configure your application´s Doctrine

Step 1: Configure the composer.json file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add in your composer.json:

.. code-block:: json

    {
        "require": {
            "texyon/database-brain-bundle": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url": "git@github.com:TEXYONGAMES/texyonDatabaseBrainBundle.git"
            }
        ],
        "config": {
            "bin-dir": "bin",
            "github-oauth":{
                "github.com":"c1e90cd60562ff8f75d1b2bee5c69a14a8d4b6ec"
            }
        }
    }



Step 2: Download texyonDatabaseBrainBundle using composer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Require the bundle with composer:

.. code-block:: bash

    $ composer require texyon/database-brain-bundle "dev-master"

Composer will install the bundle to your project's ``vendor/texyon/database-brain-bundle`` directory.

Step 3: Enable the bundle
~~~~~~~~~~~~~~~~~~~~~~~~~

Enable the bundle in the kernel::

    <?php
    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Texyon\Database\BrainBundle\TexyonDatabaseBrainBundle(),
            // ...
        );
    }

Step 4: Configure your application´s Doctrine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: yaml

    # src/AppBundle/config/config.yml        

    doctrine:
        dbal:
            mapping_types:
                enum: string

 
