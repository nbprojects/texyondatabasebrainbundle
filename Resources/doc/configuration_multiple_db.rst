TexyonDatabaseBrainBundle Configuration Reference for multiple Entity Managers and Connections
===============================================================================================

All available configuration options are listed below with their default values.

.. code-block:: yaml

    # Doctrine Configuration
    doctrine:
        dbal:
            default_connection:   portal
            connections:
                portal:
                    driver:   pdo_mysql
                    host:     "%database_host_portal%"
                    port:     "%database_port_portal%"
                    dbname:   "%database_name_portal%"
                    user:     "%database_user_portal%"
                    password: "%database_password_portal%"
                    charset:  UTF8
                    mapping_types: 
                        enum: string
                brain:
                    driver:   pdo_mysql
                    host:     "%database_host_brain%"
                    port:     "%database_port_brain%"
                    dbname:   "%database_name_brain%"
                    user:     "%database_user_brain%"
                    password: "%database_password_brain%"
                    charset:  UTF8
        orm:
            auto_generate_proxy_classes: "%kernel.debug%"
            default_entity_manager:   portal
            entity_managers:
                default:
                    connection:       portal
                    mappings:
                        TexyonDatabasePortalBundle: ~
                        TexyonDatabaseGlobalBundle: ~
                brain:
                    connection:       brain
                    mappings:
                        TexyonDatabaseBrainBundle: ~