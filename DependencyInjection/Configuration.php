<?php

namespace Texyon\Database\BrainBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('texyon');

        $rootNode
            ->children()
                ->scalarNode('mock')->defaultValue(false)->end()              
                ->end();


        return $treeBuilder;
    }
}
