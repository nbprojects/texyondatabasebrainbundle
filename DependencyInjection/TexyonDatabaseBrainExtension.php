<?php

namespace Texyon\Database\BrainBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class TexyonDatabaseBrainExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $config = array();
        foreach ($configs as $subConfig) {
            $config = array_merge($config, $subConfig);
        }

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        if (!isset($config['ws_report_host'])) {
            $wsReportHost = null;
        } else {
            $wsReportHost = $config['ws_report_host'];
        }

        if (!isset($config['uei_host'])) {
            $ueiHost = null;
        } else {
            $ueiHost = $config['uei_host'];
        }

        
        //$container->setParameter('texyon_managers.ws_report_host', $wsReportHost);
        //$container->setParameter('texyon_managers.uei_host', $ueiHost);
    }
}
