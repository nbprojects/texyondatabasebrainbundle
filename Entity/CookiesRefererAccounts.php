<?php
namespace Texyon\Database\BrainBundle\Entity;

use Texyon\Database\BrainBundle\Entity\Model\CookiesRefererAccounts as BaseCookiesRefererAccounts;

/**
 * Description of CookiesRefererAccounts
 *
 */
class CookiesRefererAccounts extends BaseCookiesRefererAccounts
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $account;

    /**
     * @var integer
     */
    private $referer;

    /**
     * @var integer
     */
    private $level;



    /**
     * Set id
     *
     * @param integer $id
     * @return CookiesRefererAccounts
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return CookiesRefererAccounts
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set account
     *
     * @param int $account
     * @return CookiesRefererAccounts
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return int
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set referer
     *
     * @param \Texyon\Database\BrainBundle\Entity\CookiesReferer $referer
     * @return CookiesRefererAccounts
     */
    public function setReferer(\Texyon\Database\BrainBundle\Entity\CookiesReferer $referer)
    {
        $this->referer = $referer;

        return $this;
    }

    /**
     * Get referer
     *
     * @return \Texyon\Database\BrainBundle\Entity\CookiesReferer 
     */
    public function getReferer()
    {
        return $this->referer;
    }
}
