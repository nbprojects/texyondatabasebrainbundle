<?php

namespace Texyon\Database\BrainBundle\Entity;

use Texyon\Database\BrainBundle\Entity\Model\Channeling as BaseChanneling;

/**
 * Class Channeling
 * @package Texyon\Database\BrainBundle\Entity\Channeling
 */
class Channeling extends BaseChanneling
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var integer
     */
    private $account;

    /**
     * @var integer
     */
    private $level;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set channel
     *
     * @param  integer    $channel
     * @return Channeling
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Channeling
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set account
     *
     * @param int $account
     *
     * @return Channeling
     */
    public function setAccountId( $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return int
     */
    public function getAccountId()
    {
        return $this->account;
    }
}
