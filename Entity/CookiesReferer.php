<?php
namespace Texyon\Database\BrainBundle\Entity;

use Texyon\Database\BrainBundle\Entity\Model\CookiesReferer as BaseCookieReferer;

/**
 * Class CookiesReferer
 */
class CookiesReferer extends BaseCookieReferer
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $refererType;

    /**
     * @var string
     */
    private $refererName;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set refererType
     *
     * @param string $refererType
     * @return CookiesReferer
     */
    public function setRefererType($refererType)
    {
        $this->refererType = $refererType;

        return $this;
    }

    /**
     * Get refererType
     *
     * @return string 
     */
    public function getRefererType()
    {
        return $this->refererType;
    }

    /**
     * Set refererName
     *
     * @param string $refererName
     * @return CookiesReferer
     */
    public function setRefererName($refererName)
    {
        $this->refererName = $refererName;

        return $this;
    }

    /**
     * Get refererName
     *
     * @return string 
     */
    public function getRefererName()
    {
        return $this->refererName;
    }
}
