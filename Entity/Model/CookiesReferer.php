<?php

namespace Texyon\Database\BrainBundle\Entity\Model;

use Texyon\Database\BrainBundle\Entity\Model\ModelInterface;

/**
 * Class CookiesReferer
 */
class CookiesReferer implements ModelInterface
{
    /**
     * Validate required properties from entity
     */
    public function configure()
    {
        // TODO: Implement configure() method.
    }

  
    /**
     * @param string    $refererType
     * @param string    $refererName 
     *
     * @return $this
     */
    public function setCookiesReferer($refererType, $refererName)
    {
        $this->setRefererType($refererType);
        $this->setRefererName($refererName);

        return $this;
    }
}
