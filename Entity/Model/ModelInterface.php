<?php

namespace  Texyon\Database\BrainBundle\Entity\Model;

/**
 * Interface ModelInterface
 * @package Texyon\Database\BrainBundle\Entity\Model
 */
interface ModelInterface
{
    /**
     * Validate required properties from entity
     */
    public function configure();
}
