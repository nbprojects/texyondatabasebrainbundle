<?php

namespace Texyon\Database\BrainBundle\Entity\Model;

use Texyon\Database\BrainBundle\Entity\Model\ModelInterface;

/**
 * Class Channeling
 * @package Texyon\Database\BrainBundle\Entity\Model
 */
class Channeling implements ModelInterface
{
    /**
     * Validate required properties from entity
     */
    public function configure()
    {
        // TODO: Implement configure() method.
    }

    public function setNewChanneling($channelName, $level = 1)
    {
        $this->setChannel($channelName);
        $this->setLevel($level);

        return $this;
    }
}
