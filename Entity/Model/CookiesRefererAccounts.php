<?php

namespace Texyon\Database\BrainBundle\Entity\Model;

use Texyon\Database\BrainBundle\Entity\Model\ModelInterface;
use Texyon\Database\BrainBundle\Entity\CookiesReferer as CookiesRefererEntity;

/**
 * Class CookiesReferer
 * @package UserBundle\Model\Channeling
 */
class CookiesRefererAccounts implements ModelInterface
{
    /**
     * Validate required properties from entity
     */
    public function configure()
    {
        // TODO: Implement configure() method.
    }

    
    /**
     * @param int                   $accountId
     * @param CookiesRefererEntity  $referer 
     * @param int                   $level
     * @param string                $key
     *
     * @return $this
     */
    public function setCookiesRefererAccounts( $accountId, CookiesRefererEntity $referer, $level)
    {
        $this->setLevel($level);
        $this->setAccount($accountId);
        $this->setReferer($referer);

        return $this;
    }
   
}
